import pytest
import monitor
import json
from monitor import create_app

@pytest.fixture
def app():
  app = create_app()
  yield app

@pytest.fixture
def client(app):
    return app.test_client()

def test_must_have_canvas(client):
  rv = client.get('/')
  assert b'<canvas id="chart"></canvas>' in rv.data

def test_must_have_threshold(client):
  rv = client.get('/')
  assert b'<ul id="threshold"></ul>' in rv.data

def test_respond_to_cpu(client):
  rv = client.get('/cpu')
  data = json.loads(rv.get_data(as_text=True))
  assert isinstance(data, list)
  assert len(data) == 1
  assert isinstance(data[0], float)

def test_respond_to_mem(client):
  rv = client.get('/mem')
  data = json.loads(rv.get_data(as_text=True))
  assert isinstance(data, list)
  assert len(data) == 1
  assert isinstance(data[0], float)
