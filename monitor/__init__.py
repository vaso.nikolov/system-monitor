import psutil
from flask import Flask
from flask import jsonify
from flask import render_template
from flask_assets import Environment, Bundle

def create_app():
  app = Flask(__name__)
  assets = Environment(app)
  js = Bundle('jquery.js', 'chart.js')
  assets.register('js_all', js)

  @app.route("/")
  def index():
    return render_template('index.html')
  
  @app.route("/cpu", methods=['GET'])
  def cpu():
    return jsonify([(psutil.getloadavg()[0] * 100) / psutil.cpu_count()])

  @app.route("/mem", methods=['GET'])
  def mem():
    return jsonify([(psutil.virtual_memory().used * 100) / psutil.virtual_memory().total])

  return app
