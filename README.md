git clone ...  
cd system-monitor  
python3 -m venv venv  
. venv/bin/activate  
pip install -e .  
export FLASK_APP=monitor  
flask run  
